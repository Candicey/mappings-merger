package com.gitlab.candicey.mappingsmerger

import com.gitlab.candicey.mappingsmerger.mapping.NotchMapper
import com.gitlab.candicey.mappingsmerger.mapping.SeargeMapper
import java.io.File

fun main() {
    swapNotch()
}

private fun mergeSearge() {
    for (version in GameInfo.Version.entries) {
        val time = System.currentTimeMillis()
        println("Processing ${version.versionName}")

        val seargeMapper = SeargeMapper(version)

        val file = File("./outMappings/${version.versionName}.xsrg")
        file.createNewFile()

        file.bufferedWriter().use { writer ->
            seargeMapper.notchMcpClasses.forEach {
                writer.write("CL: ${it.secondName} ${it.firstName}\n")
            }

            seargeMapper.seargeMcpFields.forEach {
                writer.write("FD: ${it.secondOwner}/${it.secondName} ${it.secondDescriptor} ${it.firstOwner}/${it.firstName} ${it.firstDescriptor}\n")
            }

            seargeMapper.seargeMcpMethods.forEach {
                writer.write("MD: ${it.secondOwner}/${it.secondName} ${it.secondDescriptor} ${it.firstOwner}/${it.firstName} ${it.firstDescriptor}\n")
            }
        }

        println("Finished processing ${version.versionName} in ${System.currentTimeMillis() - time}ms")
    }
}

private fun swapNotch() {
    for (version in GameInfo.Version.entries) {
        val time = System.currentTimeMillis()
        println("Processing ${version.versionName}")

        val notchMapper = NotchMapper(version)

        val file = File("./outMappings/${version.versionName}.xsrg")
        file.createNewFile()

        file.bufferedWriter().use { writer ->
            notchMapper.classes.forEach {
                writer.write("CL: ${it.secondName} ${it.firstName}\n")
            }

            notchMapper.fields.forEach {
                writer.write("FD: ${it.secondOwner}/${it.secondName} ${it.secondDescriptor} ${it.firstOwner}/${it.firstName} ${it.firstDescriptor}\n")
            }

            notchMapper.methods.forEach {
                writer.write("MD: ${it.secondOwner}/${it.secondName} ${it.secondDescriptor} ${it.firstOwner}/${it.firstName} ${it.firstDescriptor}\n")
            }
        }

        println("Finished processing ${version.versionName} in ${System.currentTimeMillis() - time}ms")
    }
}