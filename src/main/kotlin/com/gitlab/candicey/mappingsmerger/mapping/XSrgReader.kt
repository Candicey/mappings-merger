package com.gitlab.candicey.mappingsmerger.mapping

import java.io.File

class XSrgReader(filePath: String) {
    val classMappings = mutableListOf<ClassMapping>()

    val methodMappings = mutableListOf<MethodMapping>()

    val fieldMappings = mutableListOf<FieldMapping>()

    init {
        val mappings = File(filePath)
            .bufferedReader()
            .readLines()

        mappings
            .map { it.split(": ") }
            .forEach { (type, data) ->
                when (type) {
                    "CL" -> {
                        val (firstName, secondName) = data.split(' ')
                        classMappings.add(ClassMapping(firstName, secondName))
                    }

                    "MD" -> {
                        val (firstPath, firstDescriptor, secondPath, secondDescriptor) = data.split(' ')
                        val (firstOwner, firstName) = firstPath.splitLast('/')
                        val (secondOwner, secondName) = secondPath.splitLast('/')
                        methodMappings.add(MethodMapping(
                            firstOwner, firstName, firstDescriptor,
                            secondOwner, secondName, secondDescriptor
                        ))
                    }

                    "FD" -> {
//                        val (firstPath, firstDescriptor, secondPath, secondDescriptor) = data.split(' ')
                        val split = data.split(' ')
                        val firstPath: String
                        val secondPath: String
                        if (split.size == 2) {
                            firstPath = split[0]
                            secondPath = split[1]
                        } else {
                            firstPath = split[0]
                            secondPath = split[2]
                        }
                        val (firstOwner, firstName) = firstPath.splitLast('/')
                        val (secondOwner, secondName) = secondPath.splitLast('/')
                        if (split.size == 2) {
                            fieldMappings.add(FieldMapping(firstOwner, firstName, null, secondOwner, secondName, null))
                        } else {
                            fieldMappings.add(FieldMapping(firstOwner, firstName, split[1], secondOwner, secondName, split[3]))
                        }
                    }
                }
            }
    }

    data class ClassMapping(
        val firstName: String,
        val secondName: String,
    )

    data class MethodMapping(
        val firstOwner: String,
        val firstName: String,
        val firstDescriptor: String,
        val secondOwner: String,
        val secondName: String,
        val secondDescriptor: String,
    )

    data class FieldMapping(
        val firstOwner: String,
        val firstName: String,
        val firstDescriptor: String?,
        val secondOwner: String,
        val secondName: String,
        val secondDescriptor: String?,
    )

    private fun CharSequence.splitLast(delimiter: Char): Pair<String, String> {
        val index = lastIndexOf(delimiter)
        return if (index == -1) "" to toString() else substring(0, index) to substring(index + 1)
    }
}
