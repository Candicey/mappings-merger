package com.gitlab.candicey.mappingsmerger.mapping

import com.gitlab.candicey.mappingsmerger.GameInfo

class SeargeMapper(gameVersion: GameInfo.Version) : IMapper {
    private val notchSearge by lazy { XSrgReader("./mappings/${gameVersion.versionName}-srg.xsrg") }
    private val mcpNotch by lazy { XSrgReader("./mappings/${gameVersion.versionName}-mcp.xsrg") }

    // First = Notch, Second = Searge
    private val notchSeargeMethods = notchSearge.methodMappings
    private val notchSeargeFields = notchSearge.fieldMappings

    // First = Notch, Second = MCP
    val notchMcpClasses = mcpNotch.classMappings
    private val notchMcpMethods = mcpNotch.methodMappings
    private val notchMcpFields = mcpNotch.fieldMappings

    // First = Searge, Second = MCP
    val seargeMcpMethods = notchMcpMethods.map { methodMapping ->
        XSrgReader.MethodMapping(
            notchSeargeMethods.find { it.firstOwner == methodMapping.firstOwner && it.firstName == methodMapping.firstName && it.firstDescriptor == methodMapping.firstDescriptor }?.secondOwner ?: methodMapping.firstOwner,
            notchSeargeMethods.find { it.firstOwner == methodMapping.firstOwner && it.firstName == methodMapping.firstName && it.firstDescriptor == methodMapping.firstDescriptor }?.secondName ?: methodMapping.firstName,
            notchSeargeMethods.find { it.firstOwner == methodMapping.firstOwner && it.firstName == methodMapping.firstName && it.firstDescriptor == methodMapping.firstDescriptor }?.secondDescriptor ?: methodMapping.firstDescriptor,
            methodMapping.secondOwner,
            methodMapping.secondName,
            methodMapping.secondDescriptor
        )
    }
    val seargeMcpFields = notchMcpFields.map { fieldMapping ->
        XSrgReader.FieldMapping(
            notchSeargeFields.find { it.firstOwner == fieldMapping.firstOwner && it.firstName == fieldMapping.firstName }?.secondOwner ?: fieldMapping.firstOwner,
            notchSeargeFields.find { it.firstOwner == fieldMapping.firstOwner && it.firstName == fieldMapping.firstName }?.secondName ?: fieldMapping.firstName,
            mapDescriptor(fieldMapping.secondDescriptor ?: error("Field descriptor is null")),
            fieldMapping.secondOwner,
            fieldMapping.secondName,
            fieldMapping.secondDescriptor
        )
    }

    override fun mapClass(name: String): String? = notchMcpClasses.find { it.secondName == name }?.firstName

    override fun mapMethod(owner: String, name: String, descriptor: String): MappedMethod? {
        val method = seargeMcpMethods.find { it.secondOwner == owner && it.secondName == name && it.secondDescriptor == descriptor } ?: return null
        return MappedMethod(
            method.firstOwner,
            method.firstName,
            method.firstDescriptor
        )
    }

    override fun mapField(owner: String, name: String): MappedField? {
        val field = seargeMcpFields.find { it.secondOwner == owner && it.secondName == name } ?: return null
        return MappedField(
            field.firstOwner,
            field.firstName
        )
    }

    override fun reverseMapClass(name: String): String? = notchMcpClasses.find { it.firstName == name }?.secondName

    override fun reverseMapMethod(owner: String, name: String, descriptor: String): MappedMethod? {
        val method = seargeMcpMethods.find { it.firstOwner == owner && it.firstName == name && it.firstDescriptor == descriptor } ?: return null
        return MappedMethod(
            method.secondOwner,
            method.secondName,
            method.secondDescriptor
        )
    }

    override fun reverseMapField(owner: String, name: String): MappedField? {
        val field = seargeMcpFields.find { it.firstOwner == owner && it.firstName == name } ?: return null
        return MappedField(
            field.secondOwner,
            field.secondName
        )
    }

    // map from mcp to notch
    private fun mapDescriptor(descriptor: String): String {
        val builder = StringBuilder()
        var index = 0
        while (index < descriptor.length) {
            val c = descriptor[index]
            if (c == 'L') {
                val endIndex = descriptor.indexOf(';', index)
                val className = descriptor.substring(index + 1, endIndex)
                val mappedClassName = mapClass(className) ?: className
                builder.append("L").append(mappedClassName).append(";")
                index = endIndex + 1
            } else {
                builder.append(c)
                index++
            }
        }
        val toString = builder.toString()

        return toString
    }
}
