plugins {
    kotlin("jvm") version "1.9.0"
}

group = "com.gitlab.candicey.mappingsmerger"
version = "0.1.0"

repositories {
    mavenCentral()
}

dependencies {
    testImplementation(kotlin("test"))
}

tasks.test {
    useJUnitPlatform()
}

kotlin {
    jvmToolchain(8)
}